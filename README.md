Look. BOOBIES! (.Y.)

# UA Bootstrap #

This repository is the UA's flavor of bootstrap, combining the new UA Brand with the goodness of Bootstrap 3.

## Versioning Scheme ##

Because this front end CSS framework is based on Bootstrap 3, tagged versions use a modified form of [SemVer](http://semver.org/).
All versioned releases will be tagged with a 4 point version.
The first number is the major version of bootstrap being used (3).
The following 3 numbers will be using the normal SemVer scheme (major.minor.patch).

## How to view everything
### Dependencies

npm
gulp
bower

### In the command line run:

Navigate to the folder where you keep your clone of ua-bootstrap or...
`git clone git@bitbucket.org:uadigital/ua-bootstrap.git `

```
git fetch && git checkout master
npm install
bower install
gulp
gulp build-docs
gulp serve
```

[Then go here in your browser](http://localhost:8888)