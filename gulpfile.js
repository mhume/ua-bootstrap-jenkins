'use strict'

var gulp = require('gulp')
var $ = require('gulp-load-plugins')({lazy: false})
var del = require('del')
var merge = require('merge-stream')

gulp.task('sass', function () {
  return gulp.src('./src/sass/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({ errLogToConsole: true, sourceComments: 'map', sourceMap: 'sass' }))
    .on('error', function (error) {
      console.error(error)
      this.emit('end')
    })
    .pipe($.inlineImage())
    .pipe($.autoprefixer('last 2 versions'))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('./dist/css'))
    .pipe(gulp.dest('./docs/css'))
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.minifyCss())
    .pipe(gulp.dest('./dist/css'))
})

gulp.task('js', function () {
  return gulp.src([
    'bower_components/bootstrap-sass-official/assets/javascripts/**/*',
    // 'bower_components/bootstrap-sass-official/assets/javascripts/highlight.pack.js',
    './src/js/*.js'
  ])
    .pipe($.concat('ua-bootstrap.js'))
    .pipe($.uglify())
    .pipe(gulp.dest('./dist/js/'))
    .pipe(gulp.dest('./docs/js/'))
})

gulp.task('svg', function () {
  return gulp.src('./src/svg/*.svg')
    .pipe($.svgmin())
    .pipe(gulp.dest('./src/img/.'))
})

gulp.task('jade', function () {
  return gulp.src('./docs/jade/*.jade')
    .pipe($.jade({
      pretty: true
    }))
    .pipe(gulp.dest('./docs/'))
})

gulp.task('build-docs', function () {
  var fonts = gulp.src('ua_components/ua-brand-icons/fonts/ua-brand-symbols.*')
    .pipe(gulp.dest('docs/ua-brand-icons/fonts'))
  var icons = gulp.src('ua_components/ua-brand-icons/*.css')
    .pipe(gulp.dest('docs/ua-brand-icons'))
  var hjs = gulp.src('bower_components/highlightjs/*.js')
    .pipe(gulp.dest('docs/js'))
  var holderjs = gulp.src('bower_components/holderjs/*.js')
    .pipe(gulp.dest('docs/js'))
//  var dist = gulp.src('dist/**/*')
//    .pipe(gulp.dest('docs'))
  var gc = gulp.src('bower_components/highlightjs/styles/googlecode.css')
    .pipe(gulp.dest('docs/css'))

    return merge(fonts, icons, hjs, gc, holderjs);
})

gulp.task('clean', function (cb) {
  del(['dist/*', 'dist/css'], cb)
})

gulp.task('rebuild', ['svg', 'sass', 'js', 'jade', 'build-docs'])

gulp.task('default', ['clean'], function () {
  gulp.start('rebuild')
})

gulp.task('connect', function () {
  $.connect.server({
    port: 8888,
    root: 'docs',
    livereload: true
  })
})

gulp.task('watch', function () {
  $.livereload.listen()
  gulp.watch([
    'src/sass/**/**/**',
    'src/svg/**',
    'src/js/**',
    'docs/jade/**/**/**'], ['rebuild'])
    .on('change', $.livereload.changed)
})

gulp.task('serve', ['connect', 'watch'])

gulp.task('debug', function () {
  console.log($)
})
