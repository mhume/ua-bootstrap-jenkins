## Milo Web

<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-web' data-font-name='Milo Web' data-target='.bs-example-modal-lg' data-toggle='modal' type='button'> View Sample</button>
### Normal

<style>
.normal {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-size: 36px;
  font-style: normal;
  font-weight: normal;
}
.normal span:first-child {
    text-transform:uppercase;
}
</style>

<div class="normal">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
span {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-style: normal;
  font-weight: normal;
}
```

<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-web-medium' data-font-name='Milo Web Medium' data-target='.bs-example-modal-lg' data-toggle='modal' type='button'>View Sample</button>
### Medium

<style>
.medium {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-style: normal;
  font-size: 36px;
  font-weight: 500;
}
.medium span:first-child {
    text-transform:uppercase;
}
</style>

<div class="medium">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
.medium {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-style: normal;
  font-weight: 500;
}
```
<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-web-italic' data-font-name='Milo Web Italic' data-target='.bs-example-modal-lg' data-toggle='modal' type='button'>View Sample</button>
###  Italics

<style>
.italic {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-size: 36px;
  font-style: italic;
  font-weight: normal;
}
.italic span:first-child {
    text-transform:uppercase;
}
</style>

<div class="italic">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
em {
  font-family: MiloWeb, Verdana, Geneva, sans-serif;
  font-style: italic;
  font-weight: normal;
}
```
<button class="js-specimen-modal-trigger pull-right btn btn-red" data-font-class='milo-web-bold' data-font-name='Milo Web Bold' data-target='.bs-example-modal-lg' data-toggle='modal' type='button'>View Sample</button>
### Bold

<style>
.bold {
    font-family: MiloWeb, Verdana, Geneva, sans-serif;
    font-size: 36px;
    font-style: normal;
    font-weight:bold;
}
.bold span:first-child {
    text-transform:uppercase;
}
</style>

<div class="bold">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>


```css
strong {
    font-family: MiloWeb, Verdana, Geneva, sans-serif;
    font-style: normal;
    font-weight:bold;
}
```
